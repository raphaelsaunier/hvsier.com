/* Author: Raphael Saunier

*/

$(function(){

  var showPage = function(page){
    
    //Show page
    $(".page").not($(page)).hide();
    $(page).show();
    
    $("a").removeClass("active");

    //Highlight link    
    var link = $("a[href='"+page+"']");
    link.addClass('active');

    // Highlight parents
    var d;
    if(d = link.parents('.subnav').data('parents') || link.data('parents'))
      $.each(d.split(','), function(i,item){
        $("a[rel='"+item+"']").addClass('active');
      })
    
    //Show nav
    $(".subnav").hide();
    link.parents(".nav.subnav").show();
    
    $("#"+link.attr("rel")+"-nav").show();
  }
  
  $("#header .nav a").click(function(e){
    if(!!$($(this).attr('href')).size())
      router.navigate($(this).attr('href').substr(1), true);
    return false
  });
  
  
  // Gallery 
  $("div.gallery").each(function(){
    var galleryIndex = $(this).index("div.gallery");
    var ul = $("<ul class='num'></ul>");
    var imgs = $(this).find("img");
    $(this).data("currentImage", 0);
    $(this).data("images", imgs.size()-1);
    for(var i=0; i < imgs.size(); i++){
      ul.append("<li"+(i==0?" class='active' ":"")+">"+(i+1)+"</li>");
    }
    $(this).append(ul);
    
    $(this).append('<ul class="prevnext"><li>&gt;</li><li>&lt;</li></ul>')
    $(this).find("ul.prevnext li").live("click", function(){
      var next = !$(this).index("div.gallery:eq("+galleryIndex+") ul.prevnext li");
      var currentImage = $(this).parents('div.gallery').data("currentImage");
      var images = $(this).parents('div.gallery').data("images");
      
      currentImage = next ? (((currentImage+1) > images) ? 0 : currentImage+1) 
        : (((currentImage-1) < 0) ? images : currentImage-1) ;
      
      $(this).parents('div.gallery').data("currentImage", currentImage)
      $(this).parents('div.gallery').find("ul.num li:eq("+currentImage+")").trigger("click");
    });
    
    setInterval(function(){
      $("div.gallery:eq("+galleryIndex+") ul.prevnext li:eq(0)").trigger("click");
    },6000)
    
    $(this).find("ul.num li").live("click", function(){
      var index = $(this).index("div.gallery:eq("+galleryIndex+") ul.num li");
      var imgs = $(this).parents('div.gallery').find('img');
      $("div.gallery:eq("+galleryIndex+") ul.num li").removeClass("active");
      $(this).addClass("active");
      imgs.not("img:eq("+index+")").fadeOut("slow");
      imgs.eq(index).fadeIn("slow");
    });
  });
  
  // Form validation
  $("form#swatchbookform").bind("submit", function(){
    var fields = $(this).find(".field");
    var errors = 0;
    $.each(fields, function(){
      var field = $(this);
      field.find("div.error").remove();
      if(field.attr("id") == "shadesfield"){
        if ($("#shadesfield input:checked").size() < 1){
          field.append("<div class='error'>Please choose at least one shade.</div>")
          errors++;
        }
      }
      else if(field.attr("id") == "emailfield"){
        if(!(new RegExp(/^[a-zA-Z0-9'._%+-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$/)).test(field.find('input').val()) ){
          field.append("<div class='error'>Please enter a valid email address.</div>")        
          errors++;
        }
      }
      else if(field.find('input').val() == ''){
          field.append("<div class='error'>Please enter a value for: "+field.find("input:eq(0)").attr('placeholder')+".</div>")
          errors++
        }
    });
    return !errors;
  });
  
  
  // News page
  var newsItems = $("div.newsitem");
  var currentNewsItem = 0;
  var totalNewsItems = newsItems.size();
  
  var latestNewsInterval = setInterval(function(){
    currentNewsItem = currentNewsItem >= 1 ? 0:currentNewsItem+1;
    $("#latest-news a").eq(currentNewsItem).trigger('click');
  },10000);
  
  $.each(newsItems, function(){
    var index = $(this).index("div.newsitem");
    var title = $(this).find("h2").text();
    var date = $(this).find("h4").text().replace(/^\w+\s+/, '');
    $(index == 2 ? "#archive" : "#latest-news").append("<div><h3>"+title+"</h3><p><a class='orange'>READ MORE</a> &nbsp;&nbsp;"+date+"</p></div>");
  });
  
  $("#newsitems a").click(function(e){
    if(!e.isTrigger) clearInterval(latestNewsInterval);
    $("#newsnavigation a").css('visibility', 'visible');
    var index = $(this).index("#newsitems a");
    currentNewsItem = index;
    if(currentNewsItem == 0) $("#newsnavigation a:eq(0)").css('visibility', 'hidden');
    if(currentNewsItem == totalNewsItems-1) $("#newsnavigation a:eq(1)").css('visibility', 'hidden');
    $('.newsitem').hide();
    $('.newsitem').eq(index).show();
  });
  
  $("#newsnavigation a").bind('click', function(){
    clearInterval(latestNewsInterval);
    $("#newsnavigation a").css('visibility', 'visible');
    if(currentNewsItem == 0) $("#newsnavigation a:eq(0)").css('visibility', 'hidden');
    if(currentNewsItem == totalNewsItems-1) $("#newsnavigation a:eq(1)").css('visibility', 'hidden');
    
    var index = $(this).index("#newsnavigation a");
    currentNewsItem = index == 0 ? currentNewsItem-1 : currentNewsItem+1;
    $("#newsitems a").eq(currentNewsItem).trigger('click');
  });
  
  $("#newsitems a").eq(currentNewsItem).trigger('click');  
  
  
  // Simple router
  var NavigationRouter = Backbone.Router.extend({
    routes: {
      ":page":  "page"
    },
    page: function(page) {
      showPage("#"+page);
    }
  });
  
  var router = new NavigationRouter();  
  Backbone.history.start();
  
  // Debug
  if(!window.location.hash || window.location.hash == '')
    router.navigate('home', true);
  
  $("#container").fadeIn("slow", function(){
    $("html").removeClass("loading");
  })
});






